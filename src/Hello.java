

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.ibm.json.java.JSONArray;
import com.ibm.json.java.JSONObject;


@WebServlet("/hello")
public class Hello extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	//private static final long serialVersionUID = 1L;
	public static final String HTML_START="<html><body>";
	public static final String HTML_END="</body></html>";
	public int index = 0;
	
    
    public Hello() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			handleRequest(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			handleRequest(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String sent = request.getParameter("sentence");
		String dbname = request.getParameter("db_name");
		String result = translate(sent);
		out.println(HTML_START);
		out.println("<h1>" + result + "</h1>");
		out.println(HTML_END);
		getDB(dbname).save(new Sentence(sent, result, index));
		out.println("Sentence inserted in " + dbname + " database");
		index++;
		out.println("<a href = \"index.html\" >return to index</a><br>");
	}


	private Database getDB(String db_name) {
		Database db = null;
		CloudantClient client = null;
		Scanner sc = null;
		StringBuilder VCAP_SERVICES = new StringBuilder("");
		try{
			String vcap = "";
			if((vcap = System.getenv("VCAP_SERVICES")) != null){
				VCAP_SERVICES.append(vcap);
			}
			else{
				FileReader fr = new FileReader("C:/Users/Fabio/workspace/dp1/WebContent/vcap.txt");
				sc = new Scanner(fr);
				VCAP_SERVICES.append(sc.nextLine());
				System.out.println("ecco le vcap " + VCAP_SERVICES.toString());
			}
		client = ClientBuilder.bluemix(VCAP_SERVICES.toString()).build();
		db = client.database(db_name, true);
		}catch(Exception e){
			e.printStackTrace();
		}
		return db;
	}


	private String translate(String sent) throws UnsupportedEncodingException, Exception {
		String urlReq = "https://gateway.watsonplatform.net/language-translator/api/v2/translate?source=en&target=it&text=" + URLEncoder.encode(sent, "UTF-8");
		String result = getHTML(urlReq);
		return result;
	}

	
	public static String getHTML(String urlToRead) throws Exception{
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		
		Authenticator.setDefault(new Authenticator(){
			 protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication ("93368fc7-dd9b-48bd-badd-d3bc8a373d47", "DYE6akqCI47H".toCharArray());
			 		}
			 });
		
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	      String line;
	      while ((line = rd.readLine()) != null) {
	         result.append(line);
	      }
	      rd.close();
	  return result.toString();
	}
}
