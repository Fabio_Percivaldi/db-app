
public class Sentence {

	public String sentence;
	public String frase;
	private String _id;
	
	public Sentence(String sentence, String frase, int id){
		_id = "" + id;
		this.sentence = sentence;
		this.frase = frase;
	}
	
	public String getFrase(){
		return frase;
	}
	
	public String getSentence(){
		return sentence;
	}
	
}
