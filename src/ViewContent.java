

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.ibm.json.java.JSONObject;

@WebServlet("/viewContent")
public class ViewContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Sentence sentence;
	int index;
   
    public ViewContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			handleRequest(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			handleRequest(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, InterruptedException {
		PrintWriter out = response.getWriter();
		String dbname = request.getParameter("db_name");
		index = 0;
		try{
			Database db =  getDB(dbname);
			db.info();
				try{
					while((sentence = db.find(Sentence.class, "" + index)) != null){
						out.println("frase " + index + "= " + sentence.frase);
						out.println("sentence " + index + "= " + sentence.sentence + "<br>"); 
						index++;
					}
				}catch(Exception e){
					out.println("this was the content of the db<br>");
					out.println("<a href = \"index.html\" >return to index</a><br>");
					out.println("<a href = \"indDb.html\" >return to viewContent</a>");
				} 
		}catch(Exception e)
		{
			out.print("no database with this name<br>");
			out.println("<a href = \"index.html\" >return to index</a><br>");
			out.println("<a href = \"indDb.html\" >return to viewContent</a>");
		}
		
		
	}

	private Database getDB(String db_name) {
		Database db = null;
		CloudantClient client = null;
		Scanner sc = null;
		StringBuilder VCAP_SERVICES = new StringBuilder("");
		try{
			String vcap = "";
			if((vcap = System.getenv("VCAP_SERVICES")) != null){
				VCAP_SERVICES.append(vcap);
			}
			else{
				FileReader fr = new FileReader("C:/Users/Fabio/workspace/dp1/WebContent/vcap.txt");
				sc = new Scanner(fr);
				VCAP_SERVICES.append(sc.nextLine());
			}
		client = ClientBuilder.bluemix(VCAP_SERVICES.toString()).build();
		db = client.database(db_name, false);
		}catch(Exception e){
			e.printStackTrace();
		}
		return db;
	}
}
