# README #


### What is this repository for? ###

This is my first project based on Java Liberty framework.
db-app uses the Language Translator API of Watson to translate a sentence e insert a new entry for each sentences translated into a Cloudant NoSQL database.
This application can run locally or can be deployed on Bluemix.
### How do I get set up? ###

to set up the application clone the repo with command:  `git clone https://Fabio_Percivaldi@bitbucket.org/Fabio_Percivaldi/db-app.git`

To run it locally you will need a WebSphere Application Server (it is writen to work on this runtime environment but Tomcat is supported too)

For the Bluemix deployment you need the CloudFoundry Cli installed and than to run these commands:
`cf api`
`cf login` 
`cf push "app-name" -p dp1.war`